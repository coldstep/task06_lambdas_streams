package com.task06_lambdas_streams.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

  private List<String> list = null;

  public void start() {
    list = new ArrayList<>();
    initMenu();
  }

  private void initMenu() {
    System.out.println(
        "Hi, please write "
            + "your text"
            + " when you will finish"
            + " write empty line)"
    );

    try (Scanner scanner = new Scanner(System.in)) {
      while (true) {
        String temp = scanner.nextLine();
        if (temp.isEmpty()) {
          break;
        } else {
          list.add(temp);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      System.out.println("Your statistic:");
      getResultOfAnalysis(list).forEach(System.out::println);
    }
  }

  private List<String> getResultOfAnalysis(List<String> list) {

    long count = list.stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .distinct()
        .count();

    StringBuilder sortedList = new StringBuilder("Sorted list : [");

    list.stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .distinct()
        .sorted()
        .forEach(a -> {
              sortedList
                  .append(a)
                  .append(", ");
            }
        );

    sortedList.append("]");

    Map<String, Long> map1 = list.stream()
        .flatMap(s -> Stream.of(s.split(" ")))
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    Map<String, Long> map2 = list.stream()
        .flatMap(s -> Stream.of(s.split("")))
        .filter(s -> !Character.isUpperCase(s.charAt(0)))
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    StringBuilder temp1 = new StringBuilder(
        "Occurrence number of each word in the text : \n");
    StringBuilder temp2 = new StringBuilder(
        "Occurrence number of each symbol except upper case characters:\n");

    map1.forEach((k, v) -> temp1.append(k).append("-").append(v).append(", "));
    map2.forEach((k, v) -> temp2.append(k).append("-").append(v).append(", "));

    list.add("Number of unique words - " + count);
    list.add(sortedList.toString());
    list.add(temp1.toString());
    list.add(temp2.toString());

    return list;
  }


}
