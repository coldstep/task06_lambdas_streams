package com.task06_lambdas_streams.task1;

@FunctionalInterface
public interface ThreeVariable {

  int getResult(int a, int b, int c);
}
