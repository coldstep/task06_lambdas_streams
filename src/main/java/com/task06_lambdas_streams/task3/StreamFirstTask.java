package com.task06_lambdas_streams.task3;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamFirstTask {

  public List<Integer> generateFirst() {
    return Stream
        .generate(() -> new Random().nextInt(100) + 1)
        .limit(20)
        .collect(Collectors.toList());
  }

  public List<Integer> generateSecond() {
    return Stream
        .iterate(1, (n) -> n + new Random().nextInt(20) + 1)
        .limit(20)
        .collect(Collectors.toList());
  }

  public List<Integer> generateThird() {
    Integer[] array = new Integer[100];
    Random random = new Random();
    for (int i = 0; i < array.length; i++) {
      array[i] = random.nextInt(100) + 1;
    }
    return Stream
        .of(array)
        .limit(20)
        .collect(Collectors.toList());
  }

  public void getResult(List<Integer> list) {

    IntSummaryStatistics intSummaryStatistics = list.stream()
        .mapToInt(Integer::intValue)
        .summaryStatistics();

    System.out.println(
        "Max - " + intSummaryStatistics.getMax()
            + "\nMin - " + intSummaryStatistics.getMin()
            + "\nAverage - " + intSummaryStatistics.getAverage()
            + "\nCount - " + intSummaryStatistics.getCount()
            + "\nSum - " + intSummaryStatistics.getSum()
    );

    double average = intSummaryStatistics.getAverage();

    System.out.println(
        "Count of numbers that are bigger than average - " +
            list.stream()
                .filter(a -> a > average)
                .count()
    );
  }


}
