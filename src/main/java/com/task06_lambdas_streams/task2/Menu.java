package com.task06_lambdas_streams.task2;

import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;


public class Menu {

  private Map<String, String> menu = new TreeMap<>();
  private Map<String, Command> functionalMenu = new TreeMap<>();
  private Scanner scanner = new Scanner(System.in);

  public Menu() {

    System.out.println("Welcome");
    initMenu();
    initFunctionalMenu();
    start();

  }

  private void initMenu() {
    menu.put("1 ", "Get result");
    menu.put("2 ", "Show date and time");
    menu.put("3 ", "Send message yourself");
    menu.put("4 ", "Show tech specs of MacBook Pro");
    menu.put("Q ", "Exit");
  }

  private void initFunctionalMenu() {
    functionalMenu.put("1", (a) -> {
      System.out.println(a + 500 + "$ It's your future salary (joke)");
    });
    functionalMenu.put("2", this::showRealTime);
    functionalMenu.put("3", new Command() {
      @Override
      public void execute(String value) {
        System.out.println(value);
      }
    });
    functionalMenu.put("4", new TechSpecs());
    functionalMenu.put("Q", this::quit);
  }

  private void start() {
    menu.forEach((k, v) -> System.out.println(k + v));

    System.out.println("Please make your choice");

    switch (getValueForFunction()) {
      case "1":
        System.out.println("Please write number from 1 to 10 ");
        functionalMenu.get("1").execute(getValueForFunction());
        break;
      case "2":
        functionalMenu.get("2").execute(null);
        break;
      case "3":
        System.out.println("Write message");
        functionalMenu.get("3").execute(getValueForFunction());
        break;
      case "4":
        functionalMenu.get("4").execute(null);
        break;
      case "Q":
        break;
      default:
        System.out.println("Wrong symbols!!!");
        start();
        break;
    }


  }

  private String getValueForFunction() {
    return scanner.nextLine();
  }

  private void showRealTime(String value) {
    System.out.print("Today : ");
    System.out.println(new Date());
  }

  private void quit(String exitValue) {
    System.out.println("Bye bye");
    System.exit(0);
  }

}
