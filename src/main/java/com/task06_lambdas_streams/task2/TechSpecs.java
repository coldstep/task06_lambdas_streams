package com.task06_lambdas_streams.task2;

import java.util.Scanner;

public class TechSpecs implements Command {

  @Override
  public void execute(String value) {
    setChoice();

  }

  private void setChoice() {
    System.out.println("Choose witch macBook tech spec you want to watch");
    System.out.println(""
        + "1. MacBook 12\n"
        + "2. MacBook Pro 13\n"
        + "3. MacBook Pro 15\n\n"
        + "Please Make your choice");
    switch (getValueForFunction()) {
      case "1":
        System.out.println(getInfoAboutMacBook12());
        break;
      case "2":
        System.out.println(getInfoAboutMacBookPro13());
        break;
      case "3":
        System.out.println(getInfoAboutMacBookPro15());
        break;
      default:
        System.out.println("Wrong symbols!!!");
        setChoice();
        break;
    }

  }

  private String getInfoAboutMacBook12() {
    return "    MacBook 12\n"
        + "Display : 12inch retina display\n"
        + "Processor : Intel core m5\n"
        + "Ram : 8gb\n"
        + "Memory : ssd 128gb\n"
        + "Graphics: intel hd graphics 450\n";
  }

  private String getInfoAboutMacBookPro13() {
    return "    MacBook Pro 13\n"
        + "Display : 13inch retina display with true tone\n"
        + "Processor : Intel core i5 (8gen)\n"
        + "Ram : 8-16gb ddr3\n"
        + "Memory : ssd 128gb\n"
        + "Graphics: intel hd graphics 655\n";
  }

  private String getInfoAboutMacBookPro15() {
    return "    MacBook 12\n"
        + "Display : 12inch retina display\n"
        + "Processor : Intel core m5\n"
        + "Ram : 8-32gb ddr4\n"
        + "Memory : ssd 512gb\n"
        + "Graphics:  AMD RADEON Pro VEGA 16\n";
  }

  private String getValueForFunction() {
    return new Scanner(System.in).next();
  }
}
