package com.task06_lambdas_streams.task2;

@FunctionalInterface
public interface Command {

  void execute(String value);
}
