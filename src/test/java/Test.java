public class Test {

  public static void main(String[] args) {
    System.out.println("   Test1   ");
    Test1.main(args);
    System.out.println("\n\n\n"
        + "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n"
        + "   Test2   "
        + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
    Test2.main(args);
    System.out.println("\n\n\n"
        + "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n"
        + "   Test3   "
        + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
    Test3.main(args);
    System.out.println("\n\n\n"
        + "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n"
        + "   Test4   "
        + "\n\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
    Test4.main(args);
  }
}
