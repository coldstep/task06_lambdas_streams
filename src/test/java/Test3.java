import com.task06_lambdas_streams.task3.StreamFirstTask;
import java.util.List;

public class Test3 {

  public static void main(String[] args) {
    StreamFirstTask task = new StreamFirstTask();

    List<Integer> list1 = task.generateFirst();
    List<Integer> list2 = task.generateSecond();
    List<Integer> list3 = task.generateThird();

    System.out.println("\nFirst");
    list1.forEach((i) -> System.out.print(i + " "));
    System.out.println("\nSecond");
    list2.forEach((i) -> System.out.print(i + " "));
    System.out.println("\nThird");
    list3.forEach((i) -> System.out.print(i + " "));

    System.out.println("\nResult of list1");
    task.getResult(list1);
    System.out.println("\nResult of list2");
    task.getResult(list2);
    System.out.println("\nResult of list3");
    task.getResult(list3);


  }
}
