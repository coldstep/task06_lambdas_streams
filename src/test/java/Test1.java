import com.task06_lambdas_streams.task1.ThreeVariable;

public class Test1 {

  public static void main(String[] args) {
    ThreeVariable first = (a, b, c) -> (a > b && a > c) ? a : (b > a && b > c) ? b : c;

    ThreeVariable second = (a, b, c) -> (a + b + c) / 3;

    System.out.println("First result : "+first.getResult(7, 9, 7));
    System.out.println("Second result : "+second.getResult(5, 6, 7));




  }

}
